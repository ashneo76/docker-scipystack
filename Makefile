.PHONY: build

IMAGE_NAME=ashneo76/scipystack
IMAGE_VERSION=latest
PWD=$(shell pwd)
HOST_PORT=127.0.0.1:45678
GUEST_PORT=8888

build:
	docker pull ipython/scipystack
	docker build -t $(IMAGE_NAME):$(IMAGE_VERSION) ./docker

run:
	docker run -it -h $(HOSTNAME) -p $(HOST_PORT):$(GUEST_PORT) -v $(PWD)/guest_home:/root $(IMAGE_NAME):$(IMAGE_VERSION)

init:
	docker run --rm -v $(PWD)/guest_home:/root $(IMAGE_NAME):$(IMAGE_VERSION) jupyter notebook --generate-config
	docker run --rm -v $(PWD)/guest_home:/root $(IMAGE_NAME):$(IMAGE_VERSION) jupyter serverextension enable --py jupyterlab

serve:
	docker run -d -h $(HOSTNAME) --name scipystack -p $(HOST_PORT):$(GUEST_PORT) -v $(PWD)/guest_home:/root $(IMAGE_NAME):$(IMAGE_VERSION)

commit:
	docker commit $1 $(IMAGE_NAME):$(IMAGE_VERSION)

deploy:
	docker push $(IMAGE_NAME):$(IMAGE_VERSION)
